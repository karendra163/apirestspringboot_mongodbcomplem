package mis.pruebas.apirest.servicios.impl;

import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ObjetoNoEncontrado;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import mis.pruebas.apirest.servicios.repositorios.RepositorioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {

    @Autowired
    RepositorioCuenta repositorioCuenta;


    @Override
    public List<Cuenta> obtenerCuentas() {
        return this.repositorioCuenta.findAll();
    }

    @Override
    public void insertarCuentaNueva(Cuenta cuenta) {
        this.repositorioCuenta.insert(cuenta);
    }

    @Override
    public Cuenta obtenerCuenta(String numero) {

        final var quizasCuenta = this.repositorioCuenta.findById(numero);
        if (!quizasCuenta.isPresent())
            throw new ObjetoNoEncontrado("No existe la cuenta número " + numero);
        return quizasCuenta.get();
    }

    @Override
    public void guardarCuenta(Cuenta cuenta) {

        System.err.println(String.format("==== guardarCuenta %s %.2f", cuenta.numero, cuenta.saldo));
               if (!this.repositorioCuenta.existsById(cuenta.numero))
                       throw new ObjetoNoEncontrado("No existe la cuenta número " + cuenta.numero);
               this.repositorioCuenta.save(cuenta);
    }

    @Override
    public void emparcharCuenta(Cuenta parche) {

        throw new UnsupportedOperationException("emparcharCuenta NO IMPLEMENTADO");
    }

    @Override
    public void borrarCuenta(String numero) {
        //this.repositorioCuenta.deleteById(numero);
        throw new UnsupportedOperationException("borrarCuenta NO IMPLEMENTADO");
    }
}
