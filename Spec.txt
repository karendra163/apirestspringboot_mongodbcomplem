API REST
--------

Cliente
    documento
    nombre
    edad
    fechaNacimiento
    telefono
    correo
    direccion

Cuenta
    numero: 141349803
    moneda: PEN,USD
    saldo: 0.00
    tipo: AHORRO,CTACORRIENTE
    estado: ACTIVA,SUSPENDIDA
    oficina: TEXTO

RECURSOS
========

Clientes -> CRUD (Create,Read,Update,Delete)
Cuentas -> CRUD

GET /clientes -> Colección de clientes.
    Recuperar los clientes.
    ?pagina=4&cantidad=3


POST /clientes + {CUERPO} -> Agregar un cliente.
    Guardar un cliente completo.

GET /clientes/{id} -> Un cliente en concreto.
    Recuperar un cliente específico.

PUT /clientes/{id} + {CUERPO} -> Reemplazar.
    Guardar un cliente completo.
PATCH /clientes/{id} + {CUERPO} -> Modificar levementer (emparchar).
    Guardar un cliente parcialmente.

Análogo para clientes.
